import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    data = {
        name: 'Natalia',
        positions: [
            {
                name: 'Home student',
                time: 4
            },
            {
                name: 'Ballet student',
                time: 6
            },
            {
                name: 'English student',
                time: 2
            }
        ],
    };

    form: FormGroup;

    constructor(
        private fb: FormBuilder,
    ) { }

    ngOnInit(): void {
        this.form = this.fb.group({
            name: [this.data.name, [Validators.required]],
            positions: this.fb.array([])
        });
        this.loadPositions();
    }

    loadPositions() {
        for (const position of this.data.positions) {
            this.addItem(position.name, position.time);
        }
    }

    createItem(name: string = '', time: number = 0): FormGroup {
        return this.fb.group({
            id: [ Math.floor(Math.random() * 100), []], // this flield isn't in the html
            name: [name, [Validators.required]],
            time: [time, [Validators.required, Validators.min(1)]]
        });
    }

    addItem(name: string = '', time: number = 0): void {
        const positions = this.form.get('positions') as FormArray;
        positions.push(this.createItem(name, time));
    }

    remove(i: number) {
        const controls = this.form.get('positions');
        (controls as FormArray).removeAt(i);
    }

    send() {
        console.log(this.form.value);
    }

}
